db.fruits.aggregate([
	{
		$match: {"supplier" : "Red Farms Inc."}
	},
	{
		$count: "itemsInRedFarms"
	}
])

db.fruits.aggregate([
	{
		$match: {"price" : {$gt: 50}}
	},
	{
		$count: "noOfItemsGreaterThan50"
	}
])

db.fruits.aggregate ([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id : "$supplier", avgPriceSupplier: {$avg: "$price"}}
		}
	])

db.fruits.aggregate ([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id : "$supplier", maxPriceSupplier: {$max: "$price"}}
		}
	])

db.fruits.aggregate ([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id : "$supplier", minPriceSupplier: {$min: "$price"}}
		}
	])




