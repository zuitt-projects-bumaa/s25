/*
Aggregation in mongoDB

mini activity

*/

db.fruits.insertMany([
	{
	"name": "Apple",
	"supplier": "Red Farms Inc.",
	"stocks": 20, 
	"price": 40, 
	"onSale": true
	},
	{
	"name": "Banana",
	"supplier": "Yellow Farms",
	"stocks": 15, 
	"price": 20, 
	"onSale": true
	},
	{
	"name": "Kiwi",
	"supplier": "Green Farming and Canning",
	"stocks": 25, 
	"price": 50, 
	"onSale": true
	},
	{
	"name": "Mango",
	"supplier": "Yellow Farms",
	"stocks": 10, 
	"price": 60, 
	"onSale": true
	},
	{
	"name": "Dragon Fruit",
	"supplier": "Red Farms Inc.",
	"stocks": 10, 
	"price": 60, 
	"onSale": true
	}
	])


/*
Aggregation 
	- process of generating manipulated data and perform operations to create filtered results thet helps data analysis
	-helps i  creating reports from analyzing the data provided in docs
	-helps us in ariving at summarized information not previously shown in our documents

Aggregation Pipeline:
	-aggregation is done in 2-3steps. each step is a pipeline
	1. $match operator
		-used to get docs that will meet our condition
		Syntax:
			{$match: {field: value}}
	2. $group operator
		-groups elements/docs together
		Syntax: 
			{$group:{_id : <id>, fieldResult:"valueResult"}}

			_id: this is us associating an id for aggregated result
				>>when id is provided with $supplier, we created aggregated reults per supplier

	3. $sum operator
		-gets the total of all the values of the given field
		- non-numerical values won't count
*/
db.fruits.aggregate([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id : "$supplier", totalStocks: {$sum: "$stocks"}}
		}

	])

db.fruits.aggregate([
	{
		$match: {"onsale" : true}
	},
	{
		$group: {_id : "$supplier", totalPrice: {$sum: "$price"}}
	}
])

// mini acttivity

db.fruits.aggregate([
	{
		$match: {"supplier" : "Red Farms Inc."}
	},
	{
		$group: {_id :"notForSale", totalStocks: {$sum: "$stocks"}}
	}
	])

	
// average operator 

db.fruits.aggregate ([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id : "avrPriceOnSale", avgPrice: {$avg: "$price"}}
		}
	])



// $max allows us to get the highest value out of all the values of a given field

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id: "maxStocksOnSale", maxStock: {$max: "$stocks"}}
	}
])

// $min allows us to get the lowest value out of all the values of a given field

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id: "minStockOnSale", minStock: {$min: "$stocks"}
		}
	}
])

// others pipelines:

// $count 

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$count: "itemsOnSale"
	}
])

db.fruits.aggregate([
	{
		$match: {"price" : {$gte: 60}}
	},
	{
		$count: "greaterThan60"
	}
])


// mini activity

db.fruits.aggregate([
	{
		$match: {"onSale" : false}
	},
	{
		$count: "greaterThan60"
	}
])

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id: "$supplier", noOfItems: {$sum:1}}
	}
])

// you can have multiple field result for aggregated result

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id: "$supplier", noOfItems: {$sum:1}, totalStocks: {$sum: "$stocks"}}
	}
])

// $out - save/output your aggregated result from $group in a new location

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}
	},
	{
		$out: "totalStocksPerSupplier"
	}
])

db.fruits.updateOne({"name" : "Dragon Fruit"}, {$set: {"stocks" : "10"}})